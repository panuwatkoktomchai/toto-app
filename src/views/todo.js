import React, { useState, useEffect } from 'react'
import { connect } from 'react-redux'
import { Button, Modal } from 'react-bootstrap'
import TodoCard from '../components/todoCard'
import TodoForm from '../components/todoForm'
import CallApi from '../api'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPlus } from '@fortawesome/free-solid-svg-icons'
import { notificationDispatch } from '../store/notification/action'
function TodoApp(props) {
    const [modalDelete, setModalDelete] = useState(false)
    const [modalDetail, setModalDetail] = useState(false)
    const [formCreate, setForm] = useState(false)
    const [cardId, setCardId] = useState(0)
    const [todoList, setTodoList] = useState([])
    const [itemSelect, selectItem] = useState({})
    const [formState, setStateForm] = useState(false)
    
    const saveTodoItem = async (item, key=false) => {
        console.log(item)
        if (key) {
            /**
             * Update
             */
            selectItem({})
            const response = await CallApi('PUT', '/todos/'+key, item)
            if (response.status === 200) {
                props.notification.pushNotice({
                    id: Date.now(),
                    type: 'success',
                    message: 'Update success'
                })
                setForm(false)
            }
        }else {
            /**
             * insert
             */
            const response = await CallApi('POST', '/todos/', item)
            if (response.status === 200) {
                props.notification.pushNotice({
                    id: Date.now(),
                    type: 'success',
                    message: 'Insert success'
                })
                setForm(false)
            }
        }
    }
    const getTodoList = async () => {
        selectItem({})
        const response = await CallApi('GET', '/todos/')
        setTodoList(response.data)
    }

    const deleteTodoList = async () => {
        const response = await CallApi('DELETE', '/todos/'+cardId)
        if (response.status === 200) {
            props.notification.pushNotice({
                id: Date.now(),
                type: 'success',
                message: 'Delete Todo task success'
            })
            setCardId(cardId)
            setModalDelete(false)    
        }
    }

    const editTodo = () => {
        setModalDetail(false)
        setStateForm(true)
        setForm(true)
    }

    useEffect( () => {
        if (!formCreate && !modalDelete) {
            getTodoList()
        }
    }, [formCreate, modalDelete])
    return (
        <>
        <TodoForm
        show={ formCreate }
        hideModal={ e => setForm(false) }
        onSubmitFrom={ saveTodoItem }
        edit={ formState }
        data={ itemSelect }
        />
        <Button block onClick={e => {
            setStateForm(false)
            setForm(true) 
            }} variant="primary" >
            <FontAwesomeIcon icon={ faPlus }/>
             <b>Add</b>
        </Button>
        {
            todoList.map((each, key) =>(
               <TodoCard
                key={key}
                data={each}
                showDetail={ e => {
                    selectItem(each)
                    setModalDetail(true)
                }}
                deleteTodoList={ e => {
                    setCardId(each._id)
                    setModalDelete(true)
                } }
               >
               </TodoCard>
            ))
        }

        {/* modal confirm delete */}
        <Modal show={ modalDelete } onHide={e => true }>
            <Modal.Header>Delete Todo taks</Modal.Header>
            <Modal.Body>Do you want to delete?</Modal.Body>
            <Modal.Footer>
                <Button onClick={e => setModalDelete(false) } variant="light">Cancle</Button>
                <Button onClick={e => deleteTodoList()} variant="danger">Delete</Button>
            </Modal.Footer>
        </Modal>

        {/* Modal show todo task detail */}
        <Modal centered show={ modalDetail } onHide={ e=> setModalDetail(false) }>
            <Modal.Header closeButton>
                <Modal.Title>Title: { itemSelect.title }</Modal.Title>
            </Modal.Header>
            <Modal.Body>Description: { itemSelect.description }</Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={e => setModalDetail(false)}>
                    Close
                </Button>
                <Button variant="primary" onClick={e =>  editTodo()}>
                    Edit
                </Button>
            </Modal.Footer>
        </Modal>
        </>
    )
}

export default connect(null, notificationDispatch)(TodoApp)