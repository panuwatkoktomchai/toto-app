import React, { useState } from 'react'
import { connect } from 'react-redux'
import { authenticationDispatch } from '../store/authentication/action'
import { Card, Button, Form } from 'react-bootstrap'
import { setCookie } from '../helper/cookie'

const mapStatetoProps = state => {
	return {
		auth: state.auth
	}
}

function Login(props) {

    const [username, setUsername] = useState('app-todo')
    const [password, setPassword] = useState('123456789')

    const handleFormSubmit = async (e) => {
        e.preventDefault()
        const response = await props.auth.authenticate({username, password})
        if (response.status === 200) {
            setCookie(process.env.REACT_APP_NAME, response.data.token, 1).then( () => {
				props.authentication()
			})
        }
    }

    return(
        <Card className="">
            <Card.Header>Login</Card.Header>
                <Card.Body>
                    <Form onSubmit={ handleFormSubmit }>
                        <Form.Group controlId="formBasicEmail">
                            <Form.Label>Username</Form.Label>
                            <Form.Control
                            onChange={ e => setUsername(e.target.value) } 
                            defaultValue={ username }
                            type="text" 
                            placeholder="Enter username"
                            required={ true }
                            />
                        </Form.Group>

                        <Form.Group controlId="formBasicPassword">
                            <Form.Label>Password</Form.Label>
                            <Form.Control 
                            onChange={ e => setPassword(e.target.value) } 
                            defaultValue={ password }
                            type="password" 
                            placeholder="Password" 
                            required={ true }
                            />
                        </Form.Group>
                        <Button variant="primary" type="submit">
                            Login
                        </Button>
                    </Form>
                </Card.Body>
            <Card.Footer className="text-muted">Tod app</Card.Footer>
        </Card>
    )
}

export default connect(mapStatetoProps,authenticationDispatch)(Login)