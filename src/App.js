import React, { Fragment } from 'react';
import './App.css';
import { Container } from 'react-bootstrap'
import {
	BrowserRouter as Router,
	Switch,
	Route,
	Redirect,
  } from "react-router-dom";
import { connect } from 'react-redux'

import { authenticationDispatch } from './store/authentication/action'
import Login from './views/login'
import TodoApp from './views/todo'
import { Button, Navbar } from 'react-bootstrap'
import Alert from './components/alert'

const mapStatetoProps = state => {
	return {
		auth: state.auth
	}
}

let array1 = [2, 5, 6, 8, 5, 8]
let array2 = [1, 8, 9, 7, 8, 3, 5, 5]

let newarray=[]
array1.forEach( (each1) =>{
	let stat = true
	array2.forEach(each2 => {
		if (stat && each1 === each2) {
				newarray.push(each1)
				stat = false
		}
	})
})

console.log(newarray)

function App(props) {
	return (
		<Fragment>
			<Alert></Alert>
			<Navbar className="bg-light justify-content-left">
				{	
					props.auth.isAuthenticated &&
					<Button onClick={ e => props.unAuthenticate() } variant="light">Logout</Button>
				}
			</Navbar>
			<Container>
				<Router>
					<Switch>
						{
							props.auth.isAuthenticated ?
							<>
								<Route path="/" render={ ()=> <TodoApp></TodoApp> }></Route>
								<Redirect to="/"></Redirect>
							</> :
							<>
								<Route path="/login" render={ ()=> <Login/> } ></Route>
								<Redirect to="/login"></Redirect>
							</>
						}
					</Switch>
				</Router>
			</Container>
		</Fragment>
	);
}

export default connect(mapStatetoProps, authenticationDispatch)(App);
