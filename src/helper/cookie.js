export function getCookie(cname) {
	var name = cname + "=";
	var decodedCookie = decodeURIComponent(document.cookie);
	var ca = decodedCookie.split(';');
	for(var i = 0; i <ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) === ' ') {
			c = c.substring(1);
		}
		if (c.indexOf(name) === 0) {
			let cookie = c.substring(name.length, c.length);
			return cookie
		}
	}
	return false;
}

export function setCookie(cname, cvalue, exdays) {
	return new Promise(resove => {
		let d = new Date();
		d.setTime(d.getTime() + (exdays*24*60*60*1000));
		let expires = "expires="+ d.toUTCString();
		let setcookie =  cname + "=" + cvalue + ";" + expires + ";path=/";
		document.cookie = setcookie
		resove(setcookie)
	})
}

export function deleteCookie(cname) {
	let d = new Date();
	d.setTime(d.getTime() + (-1*24*60*60*1000));
	let expires = d.toUTCString();
	document.cookie = `${cname}=''; expires=${expires}; path=/`
}