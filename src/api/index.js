import store from '../store'
import { getCookie } from '../helper/cookie'
import { authentication } from '../store/authentication/action.js'
import { notification } from '../store/notification/action'
import Axios from 'axios'

const api = async (method, path, body=null) => {
    return Axios({
        url: process.env.REACT_APP_API_ENPOINT + path,
		method: method,
		mode: 'cors',
		headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + getCookie(process.env.REACT_APP_NAME),
		},
        data: body,
    }).then( response => {
        if (response.status === 401) {
            store.dispatch({
                type: authentication.LOGOUT
            })
            return false
        }

        return response
    }).catch( e => {
        store.dispatch({
            type: authentication.LOGOUT
        })
        return false
    })
}

export default api