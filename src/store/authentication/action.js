
export const authentication = {
  LOGIN: 'LOGIN',
  LOGOUT: 'LOGOUT',
  SETUSER: 'SETUSER'
} 

export const authenticationDispatch = (dispatch) => {
	return {
		authentication: () => {
			dispatch({
				type: authentication.LOGIN,
			})
		},
		unAuthenticate: () => {
			dispatch({
				type: authentication.LOGOUT
			})
		},
		setUserlogin: (user) => {
			dispatch({
				type: authentication.SETUSER,
				payload:  user
			})
			}
		}
}