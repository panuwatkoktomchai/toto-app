import { authentication } from './action'
import { getCookie, deleteCookie } from '../../helper/cookie'
import Axios from 'axios'

const Auth = {
	clientVeryfication: () => {
		let cookie = getCookie(process.env.REACT_APP_NAME)
		if (cookie) {
			return true
		}
		return false
	},
	isAuthenticated: false,
	authenticate: (secrete) => {
		return Axios({
		url: `${process.env.REACT_APP_API_ENPOINT}/users/auth`,
		method: 'POST',
		mode: 'cors',
		headers: {
			'Content-Type': 'application/json',
		},
		data:{
			username: secrete.username,
			password: secrete.password
		}

		}).then( response => {
			return response
		}).catch( e => {
			return false
		})
	}
}

if (Auth.clientVeryfication()) {
	Auth.isAuthenticated = true
}

export const UserLoginReducer = ( state = [], action ) => {
	switch (action.type) {
		case authentication.SETUSER:
			state = action.payload
			break;
	
		default:
			break;
	}
	return state
}

export const AuthenReducer = ( state = Auth, action) => {
	switch (action.type) {
		case authentication.LOGIN:
			state = {
				...state,
				isAuthenticated: true
			}
			return state
		case authentication.LOGOUT:
			deleteCookie(process.env.REACT_APP_NAME)
			state = {
				...state,
				isAuthenticated: false
			}
			return state;
		default:
			return state;
	}
}