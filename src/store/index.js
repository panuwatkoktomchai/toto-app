import { createStore, combineReducers, applyMiddleware } from 'redux'
import { AuthenReducer } from './authentication/reducer'
import { NotificationReducer } from './notification/reducer'
const mylogger = (store)=>(next)=>(action)=>{
	next(action)
}

export default createStore(combineReducers({ 
	auth: AuthenReducer,
	notice: NotificationReducer 
}), {}, applyMiddleware(mylogger))

