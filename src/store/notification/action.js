export const notification = {
    PUSHNOTICE: 'PUSHNOTIFICATION',
    DROPNOTICE: 'DROPNOTIFICATION'
}

export const notificationDispatch = dispatch => {
    return {
        notification: {
            pushNotice: (notice) => {
                dispatch({
                    type: notification.PUSHNOTICE,
                    payload: notice 
                })

                setTimeout(() => {
                    dispatch({
                        type: notification.DROPNOTICE,
                        payload: notice.id
                    })
                }, 5000);
            },
            dropNotice: (id) => {
                dispatch({
                    type: notification.DROPNOTICE,
                    payload: id
                })
            }
        }
    }
}

