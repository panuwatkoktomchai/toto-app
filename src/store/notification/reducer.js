import { notification } from './action'
const initNotification = [
    // {
    //     id: '123',
    //     type: 'danger',
    //     title: 'ok',
    //     message: 'ok'
    // },
]
export const NotificationReducer = (state=initNotification, action) => {
    switch (action.type) {
        case notification.PUSHNOTICE:
            var oldState = state
            state = [
                ...oldState,
                action.payload
            ]
            break;
        case notification.DROPNOTICE:
            if (state.length > 0) {
                state = state.filter( alert => alert.id !== action.payload)
            }else {
                state = []
            }
            break;
        default:
            break;
    }
    return state
}