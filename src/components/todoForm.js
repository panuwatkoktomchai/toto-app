import React, { useState } from 'react'
import { Form, Modal, Button, Container} from 'react-bootstrap'
import { useForm } from 'react-hook-form'
function TodoForm(props) {
    const { register, handleSubmit, errors } = useForm()
    const [title, setTitle] = useState()
    const [description, setDescription] = useState()

    const handleFormSubmit = (e) => {
        if(props.edit) {
            if (title === '') {
                setTitle( () => props.data.title)
            }

            if (description === '') {
                setDescription( () => props.data.description)
            }
        }
        console.log(props.edit, title,description)
        props.onSubmitFrom({title, description}, props.data._id)
    }

    
    return (
        <Modal
        show={ props.show }
        size="lg"
        centered
        >
            <Modal.Header>
            <h3>{ props.edit ? 'Edit todo task' : 'New task' }</h3>
            </Modal.Header>
            <Modal.Body>
                <Container>
                    <Form onSubmit={ handleSubmit(handleFormSubmit) }>
                        <Form.Group controlId="exampleForm.ControlInput1">
                            <Form.Label>Title</Form.Label>
                            <Form.Control 
                            onChange={e => {
                                setTitle(e.target.value)
                            } } 
                            type="text" 
                            name="title"
                            ref={register({required: true})}
                            defaultValue={ props.data.title }
                            placeholder="" />
                            { errors.title && <span className="text-red">This field is required</span> }
                        </Form.Group>
                        <Form.Group controlId="exampleForm.ControlTextarea1">
                            <Form.Label>Description</Form.Label>
                            <Form.Control 
                            onChange={e => {
                                setDescription(e.target.value)
                            } } 
                            name="description"
                            ref={register({ required: true })}
                            as="textarea" 
                            defaultValue={ props.data.description }
                            rows="3" />
                            { errors.description && <span className="text-red">This field is required</span> }
                        </Form.Group>
                        <Button block variant="primary" type="submit">
                            {props.edit ? 'Update': 'Add'}
                        </Button>
                        <Button onClick={ e => {
                            setTitle('')
                            setDescription('')
                            props.hideModal()
                            } } block variant="light" type="button">
                            Cancel
                        </Button>
                    </Form>
                </Container>
            </Modal.Body>
        </Modal>
    )
}

export default TodoForm