import React from 'react'
import { Card, Button } from 'react-bootstrap'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTrash } from '@fortawesome/free-solid-svg-icons'
function TodoCard(props) {
    const { data } = props
    return (
        <Card className="mt-2">
                <Card.Body>
            <a href="/" onClick={ e => {
                e.preventDefault()
                props.showDetail()
            }}>
                    { data.title }
            </a>
                    <Button 
                    onClick={ e => props.deleteTodoList() } 
                    style={{ float: 'right' }} 
                    variant="danger" 
                    className="btn-circle">
                        <FontAwesomeIcon icon={faTrash}/>
                    </Button>
                </Card.Body>
            </Card>
    )
}

export default TodoCard