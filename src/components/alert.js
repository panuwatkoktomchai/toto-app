import { connect } from 'react-redux'
import React, { Fragment } from 'react'
import { Alert } from 'react-bootstrap'

const mapStatetoProps = state => {
    return {
        notice: state.notice
    }
}
function TodoAlert(props) {
    return (
    <div className="todo-alert">
        {
            props.notice.map((each, key) => (
                <Alert key={key} variant={each.type} >
                    <p> { each.message} </p>
                </Alert>
            ))
        }
    </div>
    );
}

export default connect(mapStatetoProps)(TodoAlert)